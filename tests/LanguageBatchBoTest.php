<?php

use Language\Config\Config;
use Language\LanguageBatchBo;
use Monolog\Handler\TestHandler;
use Monolog\Logger;
use Language\Cache\Cache;
use Language\Translator\Translator;

class LanguageBatchBoTest extends PHPUnit_Framework_TestCase
{
	private $config;
	private $api;
	private $cache;
	private $logHandler;
	private $languageBatchBo;

	protected function setUp()
	{
		$this->config = $this->buildMockConfig();
		$this->translator = $this->buildMockTranslator();
		$this->cache = $this->buildMockCache();
		$this->logHandler = new TestHandler();
		
		$this->languageBatchBo = new LanguageBatchBo($this->config, $this->translator, $this->cache);
		$this->languageBatchBo->setLogger(new Logger(__CLASS__, [$this->logHandler]));
	}

	/**
	 * @test
	 */
	public function it_requests_language_files()
	{
		$this->setupMockForLanguageFiles();
		$this->languageBatchBo->generateLanguageFiles();
	}

	/**
	 * @test;
	 */
	public function it_requests_applet_language_xmls()
	{
		$this->setupMockForLanguageXMLFiles();
		$this->languageBatchBo->generateAppletLanguageXmlFiles();
	}

	/**
	 * @test
	 */
	public function it_stores_language_files()
	{
		$this->setupMockForLanguageFiles();
		$this->setupCacheForLanguageFiles();
		$this->languageBatchBo->generateLanguageFiles();
	}

	/**
	 * @test
	 */
	public function it_stores_applet_language_xmls()
	{
		$this->setupMockForLanguageXMLFiles();
		$this->setupCacheForLanguageXMLFiles();
		$this->languageBatchBo->generateAppletLanguageXmlFiles();
	}

	/**
	 * @test
	 */
	public function it_logs_language_files_generation()
	{
		$this->setupMockForLanguageFiles();
		$this->languageBatchBo->generateLanguageFiles();
		$this->assertTrue($this->logHandler->hasRecord('Generating language files', Logger::INFO));
		$this->assertTrue($this->logHandler->hasRecord('[APPLICATION: fakeApp][LANGUAGE: de]: OK', Logger::INFO));
		$this->assertTrue($this->logHandler->hasRecord('[APPLICATION: fakeApp][LANGUAGE: fr]: OK', Logger::INFO));
		$this->assertTrue($this->logHandler->hasRecord('[APPLICATION: fakeApp][LANGUAGE: hu]: OK', Logger::INFO));
	}

	/**
	 * @test
	 */
	public function it_logs_applet_language_xml_generation()
	{
		$this->setupMockForLanguageXMLFiles();
		$this->languageBatchBo->generateAppletLanguageXmlFiles();
		$this->assertTrue($this->logHandler->hasRecord('Getting applet language XMLs', Logger::INFO));
		$this->assertTrue($this->logHandler->hasRecord('Getting JSM2_MemberApplet (memberapplet) language XMLs', Logger::INFO));
		$this->assertTrue($this->logHandler->hasRecord(' - Available languages: fr, de', Logger::INFO));
		$this->assertTrue($this->logHandler->hasRecord('[APPLET: JSM2_MemberApplet][LANGUAGE: fr]: OK', Logger::INFO));
		$this->assertTrue($this->logHandler->hasRecord('[APPLET: JSM2_MemberApplet][LANGUAGE: de]: OK', Logger::INFO));
	}

	/**
	 * @test
	 * @expectedException \Language\NoAppletLanguagesException
	 * @expectedExceptionMessage There are no available languages for the JSM2_MemberApplet applet.
	 */
	public function it_reports_if_there_are_no_languages_for_applet()
	{
		$this->setupMockForLanguageXMLFilesRainy_NoLanguages();
		$this->languageBatchBo->generateAppletLanguageXmlFiles();
	}

	private function getRootPath()
	{
		return __DIR__ . '/fakeRootPath/';
	}

	private function buildMockConfig()
	{
		$rootPath = $this->getRootPath(); 
		$config = $this->getMockBuilder(Config::class)->getMock();
		$config->method('get')
			->with($this->logicalOr(
				$this->equalTo('system.paths.root'),
				$this->equalTo('system.translated_applications')
			))
			->will($this->returnCallback(function ($key) use ($rootPath) {
				switch ($key) {
					case 'system.paths.root': return $rootPath;
					case 'system.translated_applications': return ['fakeApp' => ['de', 'fr', 'hu']];
					default: throw new RuntimeException('Invalid key: '. $key);
				}
			}));
		return $config;
	}

	private function buildMockTranslator()
	{
		return $this->getMockBuilder(Translator::class)->getMock();
	}

	private function buildMockCache()
	{
		return $this->getMockBuilder(Cache::class)->getMock();
	}

	private function setupMockForLanguageFiles()
	{
		$this->translator->expects($this->exactly(3))
			->method('getLanguageFile')
			->withConsecutive(
				[$this->equalTo('fakeApp'), $this->equalTo('de')],
				[$this->equalTo('fakeApp'), $this->equalTo('fr')],
				[$this->equalTo('fakeApp'), $this->equalTo('hu')]
			)->will($this->returnCallback(function ($application, $language) {
				return $application . '-' . $language;
			}));
	}

	private function setupCacheForLanguageFiles()
	{
		$this->cache->expects($this->exactly(3))
			->method('storeApplicationLanguageFile')
			->withConsecutive(
				[$this->equalTo('fakeApp'), $this->equalTo('de'), $this->equalTo('fakeApp-de')],
				[$this->equalTo('fakeApp'), $this->equalTo('fr'), $this->equalTo('fakeApp-fr')],
				[$this->equalTo('fakeApp'), $this->equalTo('hu'), $this->equalTo('fakeApp-hu')]
			);
	}

	private function setupMockForLanguageXMLFiles()
	{
		$this->translator->expects($this->once())
			->method('getAppletLanguages')
			->with($this->equalTo('JSM2_MemberApplet'))
			->willReturn(['fr', 'de']);
		$this->translator->expects($this->exactly(2))
			->method('getAppletLanguageFile')
			->withConsecutive(
					[$this->equalTo('JSM2_MemberApplet'), $this->equalTo('fr')],
					[$this->equalTo('JSM2_MemberApplet'), $this->equalTo('de')]
			)->will($this->returnCallback(function ($appletName, $language) {
				return $appletName . '-' . $language;
			}));
	}
	
	private function setupCacheForLanguageXMLFiles()
	{
		$this->cache->expects($this->exactly(2))
			->method('storeAppletLanguageFile')
			->withConsecutive(
				[$this->equalTo('JSM2_MemberApplet'), $this->equalTo('fr'), $this->equalTo('JSM2_MemberApplet-fr')],
				[$this->equalTo('JSM2_MemberApplet'), $this->equalTo('de'), $this->equalTo('JSM2_MemberApplet-de')]
			);
	}

	private function setupMockForLanguageXMLFilesRainy_NoLanguages()
	{
		$this->translator->expects($this->once())
			->method('getAppletLanguages')
			->with($this->equalTo('JSM2_MemberApplet'))
			->willReturn([]);
	}
}