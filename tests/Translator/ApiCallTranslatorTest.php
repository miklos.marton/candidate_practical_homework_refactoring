<?php
use Language\Translator\ApiCallTranslator;
use Language\Api\ApiCall;

class ApiCallTranslatorTest extends PHPUnit_Framework_TestCase
{

	/**
	 * @test
	 * @expectedException \Language\Translator\TranslatorException
	 * @expectedExceptionMessage Error during getting language file: (dummy-app/hu)
	 */
	public function get_language_file_api_error()
	{
		$api = $this->getMockBuilder(ApiCall::class)->getMock();
		$translator = new ApiCallTranslator($api);
		$translator->getLanguageFile('dummy-app', 'hu');
	}

	/**
	 * @test
	 * @expectedException \Language\Translator\TranslatorException
	 * @expectedExceptionMessage Getting languages for applet (dummy-applet) was unsuccessful
	 */
	public function get_applet_languages_api_error()
	{
		$api = $this->getMockBuilder(ApiCall::class)->getMock();
		$translator = new ApiCallTranslator($api);
		$translator->getAppletLanguages('dummy-applet');
	}

	/**
	 * @test
	 * @expectedException \Language\Translator\TranslatorException
	 * @expectedExceptionMessage Getting language xml for applet: (dummy-applet) on language: (de) was unsuccessful
	 */
	public function get_applet_language_file_api_error()
	{
		$api = $this->getMockBuilder(ApiCall::class)->getMock();
		$translator = new ApiCallTranslator($api);
		$translator->getAppletLanguageFile('dummy-applet', 'de');
	}
}