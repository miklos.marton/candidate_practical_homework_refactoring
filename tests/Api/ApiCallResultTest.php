<?php
use Language\Api\ApiCallResult;

class ApiCallResultTest extends PHPUnit_Framework_TestCase
{

	/**
	 * @test
	 */
	public function it_returns_data()
	{
		$data = uniqid();
		$result = new ApiCallResult([
			'status' => 'OK',
			'data' => $data
		]);
		$this->assertEquals($data, $result->getData());
	}

	/**
	 * @test
	 * @expectedException \Language\Api\ApiException
	 */
	public function it_fails_on_false()
	{
		(new ApiCallResult(false))->getData();
	}

	/**
	 * @test
	 * @expectedException \Language\Api\ApiException
	 */
	public function it_fails_on_missing_status()
	{
		(new ApiCallResult([]))->getData();
	}

	/**
	 * @test
	 * @expectedException \Language\Api\ApiException
	 */
	public function it_fails_on_wrong_status()
	{
		(new ApiCallResult([
			'status' => 'Foo',
			'data' => ''
		]))->getData();
	}

	/**
	 * @test
	 * @expectedException \Language\Api\ApiException
	 */
	public function it_fails_on_false_data()
	{
		(new ApiCallResult([
			'status' => 'OK',
			'data' => false
		]))->getData();
	}
}