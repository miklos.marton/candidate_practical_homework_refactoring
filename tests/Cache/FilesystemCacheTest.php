<?php
use Language\Cache\FilesystemCache;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;
use League\Flysystem\FilesystemInterface;

class FilesystemCacheTest extends PHPUnit_Framework_TestCase
{

	private static $rootPath = __DIR__ . '/../fakeRootPath/';

	private $cache;

	protected function setUp()
	{
		$this->cache = new FilesystemCache(new Filesystem(new Local(self::$rootPath)));
	}

	protected function tearDown()
	{
		(new Filesystem(new Local(self::$rootPath)))->deleteDir('/cache/');
	}

	/**
	 * @test
	 */
	public function it_writes_application_language_file()
	{
		$content = uniqid();
		$this->cache->storeApplicationLanguageFile('dummy-app', 'hu', $content);
		$this->assertStringEqualsFile(self::$rootPath . '/cache/dummy-app/hu.php', $content);
	}

	/**
	 * @test
	 */
	public function it_writes_applet_language_file()
	{
		$content = uniqid();
		$this->cache->storeAppletLanguageFile('dummy-applet', 'en', $content);
		$this->assertStringEqualsFile(self::$rootPath . '/cache/flash/lang_en.xml', $content);
	}

	/**
	 * @test
	 * @expectedException \Language\Cache\CacheException
	 * @expectedExceptionMessage Unable to save application: (dummy-app) language: (hu)
	 */
	public function it_reports_if_language_file_can_not_be_stored()
	{
		$cache = new FilesystemCache($this->buildUnwritableMockFilesystem());
		$cache->storeApplicationLanguageFile('dummy-app', 'hu', 'it-should-fail');
	}

	/**
	 * @test
	 * @expectedException \Language\Cache\CacheException
	 * @expectedExceptionMessage Unable to save applet: (dummy-applet) language: (en)
	 */
	public function it_reports_if_applet_language_xml_can_not_be_stored()
	{
		$cache = new FilesystemCache($this->buildUnwritableMockFilesystem());
		$cache->storeAppletLanguageFile('dummy-applet', 'en', 'it-should-fail');
	}

	private function buildUnwritableMockFilesystem()
	{
		return $this->getMockBuilder(FilesystemInterface::class)->getMock();
	}
}