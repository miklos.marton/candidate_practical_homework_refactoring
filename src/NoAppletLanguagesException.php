<?php
namespace Language;

class NoAppletLanguagesException extends \UnexpectedValueException
{

	private $appletId;

	public function __construct($appletId, $previous = null)
	{
		parent::__construct(sprintf('There are no available languages for the %s applet.', $appletId), null, $previous);
		$this->appletId = $appletId;
	}

	public function getAppletId()
	{
		return $this->appletId;
	}
}