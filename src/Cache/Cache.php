<?php
namespace Language\Cache;

interface Cache
{

	public function storeApplicationLanguageFile($application, $language, $content);

	public function storeAppletLanguageFile($applet, $language, $content);
}