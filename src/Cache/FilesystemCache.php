<?php
namespace Language\Cache;

use League\Flysystem\FilesystemInterface;

final class FilesystemCache implements Cache
{

	private $filesystem;

	public function __construct(FilesystemInterface $filesystem)
	{
		$this->filesystem = $filesystem;
	}

	public function storeApplicationLanguageFile($application, $language, $content)
	{
		$filePath = $this->getLanguageCachePath($application, $language);
		if (! $this->filesystem->put($filePath, $content)) {
			throw new CacheException(sprintf('Unable to save application: (%s) language: (%s)', $application, $language));
		}
	}

	public function storeAppletLanguageFile($applet, $language, $content)
	{
		$filePath = $this->getAppletLanguageCachePath($language);
		if (! $this->filesystem->put($filePath, $content)) {
			throw new CacheException(sprintf('Unable to save applet: (%s) language: (%s)', $applet, $language));
		}
	}

	private function getLanguageCachePath($application, $language)
	{
		return '/cache/' . $application . '/' . $language . '.php';
	}

	private function getAppletLanguageCachePath($language)
	{
		return '/cache/flash/lang_' . $language . '.xml';
	}
}