<?php
namespace Language;

use Language\Config\Config;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;
use Psr\Log\NullLogger;
use Psr\Log\LoggerInterface;
use Language\NoAppletLanguagesException;
use Language\Cache\Cache;
use Language\Translator\Translator;
use Language\Api\StaticApiCall;

final class LanguageBatchBo
{

	private $translator;

	private $config;

	private $cache;

	private $logger;

	public function __construct(Config $config = null, Translator $translator = null, Cache $cache = null)
	{
		$this->config = $config ?: new \Language\Config\StaticConfig();
		$this->translator = $translator ?: new \Language\Translator\ApiCallTranslator(new StaticApiCall());
		$this->cache = $cache ?: new \Language\Cache\FilesystemCache(new Filesystem(new Local($this->config->get('system.paths.root'))));
		$this->logger = new NullLogger();
	}

	public function setLogger(LoggerInterface $logger)
	{
		$this->logger = $logger;
	}

	public function generateLanguageFiles()
	{
		$this->logger->info('Generating language files');
		foreach ($this->config->get('system.translated_applications') as $application => $languages) {
			$this->generateLanguageFilesForApplication($application, $languages);
		}
	}

	public function generateAppletLanguageXmlFiles()
	{
		$applets = array(
			'memberapplet' => 'JSM2_MemberApplet'
		);
		
		$this->logger->info('Getting applet language XMLs');
		
		foreach ($applets as $directory => $appletId) {
			$this->logger->info(sprintf('Getting %s (%s) language XMLs', $appletId, $directory));
			$languages = $this->getAppletLanguages($appletId);
			$this->logger->info(sprintf(' - Available languages: %s', implode(', ', $languages)));
			$this->generateLanguageFilesForApplet($appletId, $languages);
		}
	}

	private function generateLanguageFilesForApplication($application, array $languages)
	{
		foreach ($languages as $language) {
			$content = $this->translator->getLanguageFile($application, $language);
			$this->cache->storeApplicationLanguageFile($application, $language, $content);
			$this->logger->info(sprintf('[APPLICATION: %s][LANGUAGE: %s]: OK', $application, $language));
		}
	}

	private function generateLanguageFilesForApplet($applet, array $languages)
	{
		foreach ($languages as $language) {
			$content = $this->translator->getAppletLanguageFile($applet, $language);
			$this->cache->storeAppletLanguageFile($applet, $language, $content);
			$this->logger->info(sprintf('[APPLET: %s][LANGUAGE: %s]: OK', $applet, $language));
		}
	}

	private function getAppletLanguages($applet)
	{
		$languages = $this->translator->getAppletLanguages($applet);
		if (empty($languages)) {
			throw new NoAppletLanguagesException($applet);
		}
		return $languages;
	}
}
