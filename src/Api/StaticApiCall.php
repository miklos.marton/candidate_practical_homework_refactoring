<?php
namespace Language\Api;

use Language\ApiCall as Delegate;

final class StaticApiCall implements ApiCall
{

	public function call($target, $mode, $getParameters, $postParameters)
	{
		return Delegate::call($target, $mode, $getParameters, $postParameters);
	}
}