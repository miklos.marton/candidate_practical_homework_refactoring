<?php
namespace Language\Api;

interface ApiCall
{

	public function call($target, $mode, $getParameters, $postParameters);
}