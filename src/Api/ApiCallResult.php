<?php
namespace Language\Api;

final class ApiCallResult
{

	private $result;

	public function __construct($result)
	{
		$this->result = $result;
	}

	public function getData()
	{
		$this->assertCorrectResult();
		
		return $this->result['data'];
	}

	private function assertCorrectResult()
	{
		$this->assertCallSucceeded();
		$this->assertStatusIsOK();
		$this->assertContentIsNotFalse();
	}

	private function assertCallSucceeded()
	{
		if ($this->result === false || ! isset($this->result['status'])) {
			throw new ApiException('Error during the api call');
		}
	}

	private function assertStatusIsOK()
	{
		if ($this->result['status'] != 'OK') {
			$message = [
				'Wrong response:'
			];
			if (! empty($this->result['error_type'])) {
				$message[] = 'Type(' . $this->result['error_type'] . ')';
			}
			if (! empty($this->result['error_code'])) {
				$message[] = 'Code(' . $this->result['error_code'] . ')';
			}
			if (! empty($this->result['data'])) {
				$message[] = (string) $this->result['data'];
			}
			throw new ApiException(implode(' ', $message));
		}
	}

	private function assertContentIsNotFalse()
	{
		if ($this->result['data'] === false) {
			throw new ApiException('Wrong content!');
		}
	}
}