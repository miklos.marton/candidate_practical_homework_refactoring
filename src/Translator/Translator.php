<?php
namespace Language\Translator;

interface Translator
{

	public function getLanguageFile($application, $language);

	public function getAppletLanguages($applet);

	public function getAppletLanguageFile($applet, $language);
}