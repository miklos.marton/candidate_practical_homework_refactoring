<?php
namespace Language\Translator;

use Language\Api\ApiCall;
use Language\Api\ApiCallResult;

final class ApiCallTranslator implements Translator
{

	private $api;

	public function __construct(ApiCall $api)
	{
		$this->api = $api;
	}

	public function getLanguageFile($application, $language)
	{
		$result = new ApiCallResult($this->api->call('system_api', 'language_api', array(
			'system' => 'LanguageFiles',
			'action' => 'getLanguageFile'
		), array(
			'language' => $language
		)));
		
		try {
			return $result->getData();
		} catch (\Exception $e) {
			throw new TranslatorException('Error during getting language file: (' . $application . '/' . $language . ')');
		}
	}

	public function getAppletLanguages($applet)
	{
		$result = new ApiCallResult($this->api->call('system_api', 'language_api', array(
			'system' => 'LanguageFiles',
			'action' => 'getAppletLanguages'
		), array(
			'applet' => $applet
		)));
		
		try {
			return $result->getData();
		} catch (\Exception $e) {
			throw new TranslatorException('Getting languages for applet (' . $applet . ') was unsuccessful ', null, $e);
		}
	}

	public function getAppletLanguageFile($applet, $language)
	{
		$result = new ApiCallResult($this->api->call('system_api', 'language_api', array(
			'system' => 'LanguageFiles',
			'action' => 'getAppletLanguageFile'
		), array(
			'applet' => $applet,
			'language' => $language
		)));
		
		try {
			return $result->getData();
		} catch (\Exception $e) {
			throw new TranslatorException('Getting language xml for applet: (' . $applet . ') on language: (' . $language . ') was unsuccessful', null, $e);
		}
	}
}