<?php
namespace Language\Config;

use Language\Config as Delegate;

final class StaticConfig implements Config
{

	public function get($key)
	{
		return Delegate::get($key);
	}
}