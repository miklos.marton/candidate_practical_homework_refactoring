<?php
namespace Language\Config;

interface Config
{

	public function get($key);
}